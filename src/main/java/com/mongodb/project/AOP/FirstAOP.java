package com.mongodb.project.AOP;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * CreatedBy vipulpandey on 8/12/18
 **/
@Aspect
@Component
public class FirstAOP {
    @Before("execution(* com.mongodb.project.service.DataFileService.putFile())")
    public void beforePutFile(){
        System.out.println("I am test and I will be called before");
    }

    @After("execution(* com.mongodb.project.service.DataFileService.putFile())")
    public void AfterPutFile(){
        System.out.println("I am test and I will be called Afters");
    }
}
