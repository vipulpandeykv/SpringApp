package com.mongodb.project.conf;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * CreatedBy vipulpandey on 14/2/19
 **/
@Configuration

public class ElasticSearchConfig {
    @Value("${spring.data.elasticsearch.cluster-host}")
    private String elasticsearchHost;

    @Value("${spring.data.elasticsearch.cluster-port}")
    private int elasticsearchPort;

    @Value("${spring.data.elasticsearch.cluster-name}")
    private String clusterName;

    @Value("${spring.data.elasticsearch.scheme}")
    private String scheme;


    @Bean
    public RestHighLevelClient elasticSearchTemplate() throws Exception {
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(elasticsearchHost, elasticsearchPort, scheme)).setMaxRetryTimeoutMillis(50000)
        );
        return client;
    }

}
