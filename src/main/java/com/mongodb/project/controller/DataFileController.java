package com.mongodb.project.controller;

import com.mongodb.project.domain.DataFile;
import com.mongodb.project.domain.Sales;
import com.mongodb.project.service.DataFileService;
import org.apache.http.client.ResponseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.BindingResultUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CreatedBy vipulpandey on 8/12/18
 **/
@RestController
public class DataFileController {
    @Autowired
    private DataFileService dataFileService;
    public static final String CANDLE_DATA =  "/candle_data";

    @PutMapping(CANDLE_DATA+"/{id}")
    public DataFile updateDataTest(@PathVariable Long id) throws Exception{
        return dataFileService.updateDataTest(id);
    }
    @PostMapping(CANDLE_DATA)
    public String saveDataTest(){
        return dataFileService.saveDataTest();
    }


    @GetMapping(CANDLE_DATA)
    public long syncDataInElasticSearch(){
        return dataFileService.syncDataInElasticSearch();
    }
    @GetMapping("/sms")
    public Object sendMessage(){
        return dataFileService.sendMessage();
    }

    @PostMapping("/sales")
    public Object saveSales() throws Exception {

        return dataFileService.saveSales();
    }

    @PutMapping("/sales/{id}")
    public Object updateSales(@Valid @RequestBody Sales sales) {
        return dataFileService.updateSales(sales);
    }


}
