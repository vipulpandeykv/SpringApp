package com.mongodb.project.domain;

import javax.persistence.*;
import java.util.Hashtable;
import java.util.Map;

/**
 * CreatedBy vipulpandey on 8/12/18
 **/

@Entity
public class DataFile {

    @Id
    private String id;
    @Embedded
    private Map<String, Datas> datas = new Hashtable<>();
    private boolean archived = false;
    private String name;
    @Version
    private Long version;
    public String getId() {
        return id;
    }

    public DataFile() {
    }

    public DataFile(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Datas> getDatas() {
        return datas;
    }

    public void setDatas(Map<String, Datas> datas) {
        this.datas = datas;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Column(name="OPTLOCK")
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DataFile{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", version=").append(version);
        sb.append('}');
        return sb.toString();
    }
}