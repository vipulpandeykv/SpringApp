package com.mongodb.project.domain;

import org.springframework.data.annotation.Version;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * CreatedBy vipulpandey on 8/12/18
 **/
@Entity
public class Datas  {
    @Id
    private Long id;
    @NotBlank(message = "Please add some value in datas")
    private String value;
    private LocalDateTime date = LocalDateTime.now();
    private boolean archived;
    @Version
    private Long version;

    public Datas() {
    }

    public Datas(Long id, String value, LocalDateTime date, boolean archived) {
        this.id = id;
        this.value = value;
        this.date = date;
        this.archived = archived;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}