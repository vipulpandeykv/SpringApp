package com.mongodb.project.domain;

import javax.persistence.*;

import javax.persistence.Entity;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Entity
public class GraphDetails {
    @Id()
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank(message = "internal broken")
    private String name;
    @Valid
    @OneToOne(cascade = CascadeType.ALL)
    private Datas datas;

    public GraphDetails() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GraphDetails(String name) {
        this.name = name;
    }

    public Datas getDatas() {
        return datas;
    }

    public void setDatas(Datas datas) {
        this.datas = datas;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GraphDetails{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
