package com.mongodb.project.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@IdClass(MarketCandleDataCompositeId.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketCandleData {
    @Id
	private String market;
	@Id
	private String timeInterval;
	@Id
	private Long timestamp;
	@Column(columnDefinition = "DECIMAL(20,8) default '0'")
	private BigDecimal open;
	@Column(columnDefinition = "DECIMAL(20,8) default '0'")
	private BigDecimal close;
	@Column(columnDefinition = "DECIMAL(20,8) default '0'")
	private BigDecimal high;
	@Column(columnDefinition = "DECIMAL(20,8) default '0'")
	private BigDecimal low;
	@Column(columnDefinition = "DECIMAL(20,8) default '0'")
	private BigDecimal volume;

	public String getMarket() {
		return market;
	}

	public String getTimeInterval() {
		return timeInterval;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public BigDecimal getOpen() {
		return open;
	}

	public BigDecimal getClose() {
		return close;
	}

	public BigDecimal getHigh() {
		return high;
	}

	public BigDecimal getLow() {
		return low;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public void setTimeInterval(String timeInterval) {
		this.timeInterval = timeInterval;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public void setOpen(BigDecimal open) {
		this.open = open;
	}

	public void setClose(BigDecimal close) {
		this.close = close;
	}

	public void setHigh(BigDecimal high) {
		this.high = high;
	}

	public void setLow(BigDecimal low) {
		this.low = low;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MarketCandleData{");
        sb.append("market='").append(market).append('\'');
        sb.append(", timeInterval='").append(timeInterval).append('\'');
        sb.append(", timestamp=").append(timestamp);
        sb.append(", open=").append(open);
        sb.append(", close=").append(close);
        sb.append(", high=").append(high);
        sb.append(", low=").append(low);
        sb.append(", volume=").append(volume);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MarketCandleData that = (MarketCandleData) o;
        return Objects.equals(market, that.market) &&
                Objects.equals(timeInterval, that.timeInterval) &&
                Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {

        return Objects.hash(market, timeInterval, timestamp);
    }
}
