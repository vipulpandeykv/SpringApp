package com.mongodb.project.domain;

import java.io.Serializable;

/**
 * CreatedBy vipulpandey on 22/3/19
 **/
public class MarketCandleDataCompositeId implements Serializable {
    private String market;
    private String timeInterval;
    private Long timestamp;
    public String getMarket() {
        return market;
    }
    public void setMarket(String market) {
        this.market = market;
    }
    public String getTimeInterval() {
        return timeInterval;
    }
    public void setTimeInterval(String timeInterval) {
        this.timeInterval = timeInterval;
    }
    public Long getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }



    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((market == null) ? 0 : market.hashCode());
        result = prime * result + ((timeInterval == null) ? 0 : timeInterval.hashCode());
        result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MarketCandleDataCompositeId other = (MarketCandleDataCompositeId) obj;
        if (market == null) {
            if (other.market != null)
                return false;
        } else if (!market.equals(other.market))
            return false;
        if (timeInterval == null) {
            if (other.timeInterval != null)
                return false;
        } else if (!timeInterval.equals(other.timeInterval))
            return false;
        if (timestamp == null) {
            if (other.timestamp != null)
                return false;
        } else if (!timestamp.equals(other.timestamp))
            return false;
        return true;
    }



}
