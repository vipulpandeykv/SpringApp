package com.mongodb.project.domain;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
public class Sales {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @Valid
    private List<GraphDetails> graphDetails;
    @NotBlank(message = "external")
    private String name;

    public Sales(List<GraphDetails> graphDetails) {
        this.graphDetails = graphDetails;
    }

    public Sales() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<GraphDetails> getGraphDetails() {
        return graphDetails;
    }

    public void setGraphDetails(List<GraphDetails> graphDetails) {
        this.graphDetails = graphDetails;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Sales{");
        sb.append("id=").append(id);
        sb.append(", graphDetails=").append(graphDetails);
        sb.append('}');
        return sb.toString();
    }
}
