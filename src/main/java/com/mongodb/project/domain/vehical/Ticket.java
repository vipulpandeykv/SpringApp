package com.mongodb.project.domain.vehical;

import java.util.Date;

public class Ticket {

    private String ticketNumber;
    private Date parkedTime;
    private double cost;

    public Ticket() {
    }

    public Ticket(String ticketNumber, Date parkedTime, double cost) {
        this.ticketNumber = ticketNumber;
        this.parkedTime = parkedTime;
        this.cost = cost;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public Date getParkedTime() {
        return parkedTime;
    }

    public void setParkedTime(Date parkedTime) {
        this.parkedTime = parkedTime;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
