package com.mongodb.project.domain.vehical;

public class Vehicle {
    private String registrationNumber;
    private String name;
    private String type;
    private double weight;
    private Ticket ticket;

    public Vehicle() {
    }

    public Vehicle(String registrationNumber, String name, String type, double weight, Ticket ticket) {
        this.registrationNumber = registrationNumber;
        this.name = name;
        this.type = type;
        this.weight = weight;
        this.ticket = ticket;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Vehicle{");
        sb.append("registrationNumber='").append(registrationNumber).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", weight=").append(weight);
        sb.append(" ticketNumber}").append(ticket.getTicketNumber());

        return sb.toString();
    }
}
