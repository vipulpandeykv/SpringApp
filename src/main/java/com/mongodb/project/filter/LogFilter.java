package com.mongodb.project.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * CreatedBy vipulpandey on 26/3/19
 **/
public class LogFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("filter is initilized to ");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String ipAddress=request.getRemoteAddr();
        System.out.printf("Request is coming from IP address {}",ipAddress);
        chain.doFilter(request,response);
    }

    @Override
    public void destroy() {

    }
}
