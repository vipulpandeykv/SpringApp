package com.mongodb.project.repo;

import com.mongodb.project.domain.DataFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * CreatedBy vipulpandey on 8/12/18
 **/
@Repository
public interface DataFileRepo extends JpaRepository<DataFile, Serializable> {

}
