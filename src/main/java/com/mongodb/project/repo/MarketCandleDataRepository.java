package com.mongodb.project.repo;

import com.mongodb.project.domain.MarketCandleData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface MarketCandleDataRepository extends JpaRepository<MarketCandleData, Serializable> {


	@Query(value = "SELECT * FROM market_candle_data where market= ?1 AND time_interval=?2 order by time_interval LIMIT ?3, ?4 ", nativeQuery = true)
	List<MarketCandleData> findByMarketAndTimeInterval(String market, String timeInterval, int skip, int limit);

}
