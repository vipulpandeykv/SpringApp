package com.mongodb.project.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.project.domain.DataFile;
import com.mongodb.project.domain.GraphDetails;
import com.mongodb.project.domain.MarketCandleData;
import com.mongodb.project.domain.Sales;
import com.mongodb.project.domain.vehical.Ticket;
import com.mongodb.project.domain.vehical.Vehicle;
import com.mongodb.project.repo.DataFileRepo;
import com.mongodb.project.repo.MarketCandleDataRepository;
import com.mongodb.project.repo.SalesRepository;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

/**
 * CreatedBy vipulpandey on 8/12/18
 **/
@Service
@Transactional
public class DataFileService {
    // "ETHUSD", "XRPUSD", "BCHUSD", "LTCUSD", "XLMUSD",
    //            "ZECUSD", "ETHBTC", "XRPBTC", "LTCBTC", "BCHBTC", "XLMBTC", "ZECBTC", "XLMETH"
    public static final String[] EXTERNAL_MARKET = {"BTCUSD"};
    public static final Integer ELASTIC_SEARCH_SIZE = 10_000;
    public static final String ONE_MONTH = "1M";
    public static final String FOURTEEN_DAYS = "14D";
    public static final String DAY_SUFFIX = "D";
    public static final String HOUR_SUFFIX = "h";
    public static final String MINUTE_SUFFIX = "m";

    public static final String[] ACTIVE_TIME_FRAMES = {12 + HOUR_SUFFIX, 1 + DAY_SUFFIX,
            7 + DAY_SUFFIX, FOURTEEN_DAYS, ONE_MONTH};
    public static final String ACCOUNT_SID = "ACde84e4c39846cc7186ddd2f5d572ffba";
    public static final String AUTH_TOKEN = "50de41a229cbcfc855409037a4de257f";
    private static final Logger logger = LoggerFactory.getLogger(DataFileService.class);
    private static List<Vehicle> vehicles = new ArrayList<>();
    int j = 0;
    @Autowired
    DataFileRepo dataFileRepo;
    @Autowired
    RestHighLevelClient restHighLevelClient;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MarketCandleDataRepository marketCandleDataRepository;
    @Autowired
    private SalesRepository salesRepository;
    private ExecutorService executors = Executors.newFixedThreadPool(12);

    private List<MarketCandleData> getDataFromMysql(String market, String timeFrame, int skip, int limit) {
        return marketCandleDataRepository.findByMarketAndTimeInterval(market, timeFrame, skip, limit);
    }

    public Long syncDataInElasticSearch() {
        AtomicLong total = new AtomicLong();

        try {

            int pageSize = 10000;
            for (String market : EXTERNAL_MARKET) {
                Long currentMarketCount = new Long(0);
                for (String timeFrame : ACTIVE_TIME_FRAMES) {
                    if (timeFrame.equals("1m")) {
                        //timeFrame="1min";
                        continue;
                    }

                    int i = 0;
                    List<MarketCandleData> marketCandleData = getDataFromMysql(market, timeFrame, i, pageSize);
                    // added in main counter
                    total.getAndAdd(marketCandleData.size());

                    logger.info("Data found {} ", marketCandleData.size());

                    if (marketCandleData.size() > 0) {
                        //save to file
                        saveData(marketCandleData, market, timeFrame, i);
                        // increment i to size
                        i = i + marketCandleData.size();

                        currentMarketCount += marketCandleData.size();
                        total.getAndAdd(marketCandleData.size());
                        logger.info("counter inc to {} ", i);


                        while (marketCandleData.size() > 0) {
                            // again fetch from data base new records
                            marketCandleData.clear();  // cleared the data
                            marketCandleData = getDataFromMysql(market, timeFrame, i, pageSize);
                            if (marketCandleData.size() > 0) {
                                logger.info("More Data found  {} for {}", market, marketCandleData.size());
                                saveData(marketCandleData, market, timeFrame, i);
                                i = i + marketCandleData.size();
                                currentMarketCount += marketCandleData.size();
                            }
                            total.getAndAdd(marketCandleData.size());

                        }
                    }
                    logger.info("Data saved for {} {} {} ", currentMarketCount, timeFrame, market);
                }
                logger.info("Successfully written data for  {} {}", market, total.get());
            }
        } catch (Throwable e) {
            logger.info(e + "");
        }
        return total.get();
    }

    public void saveData(List<MarketCandleData> marketCandleDataList, String market, String timeFrame, int i) {
        executors.submit(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName);

            Iterator<MarketCandleData> itr = marketCandleDataList.iterator();
            if (i % 80000 == 0) {
                j++;
            }
            FileWriter file = null;
            try {
                File directory = new File("/opt/json/" + market + LocalDate.now());
                if (!directory.exists()) {
                    directory.mkdir();
                }
                file = new FileWriter("/opt/json/" + market + "/" + market + "-" + timeFrame + "-" + j + ".json", true);

                while (itr.hasNext()) {
                    org.json.simple.JSONObject jsonObject = getDataAsJSON(itr.next());
                    String str = "{\"index\": {\"_type\":\"" + market.toLowerCase() + "\"}}" + "\n" + jsonObject.toJSONString() + "\n";
                    file.write(str);
                }
            } catch (Exception e) {
                logger.info("FOF");
            } finally {
                if (file != null) {
                    try {
                        file.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private org.json.simple.JSONObject getDataAsJSON(MarketCandleData marketCandleData) {
        org.json.simple.JSONObject response = new org.json.simple.JSONObject();
        try {
            response.put("market", marketCandleData.getMarket().toLowerCase());
            response.put("timeInterval", marketCandleData.getTimeInterval());
            response.put("timestamp", marketCandleData.getTimestamp().doubleValue());
            response.put("created", new Date(marketCandleData.getTimestamp()));
            response.put("open", marketCandleData.getOpen().doubleValue());
            response.put("high", marketCandleData.getHigh().doubleValue());
            response.put("close", marketCandleData.getClose().doubleValue());
            response.put("low", marketCandleData.getLow().doubleValue());
            response.put("volume", marketCandleData.getVolume().doubleValue());

        } catch (Exception e) {

        }
        return response;
    }

    public void saveDataInElasticSearch(List<MarketCandleData> marketCandleDataList, String timeFrame) {
        Iterator<MarketCandleData> itr = marketCandleDataList.iterator();
        BulkRequest bulkRequest = new BulkRequest();
        while (itr.hasNext()) {

            IndexRequest indexRequest = new IndexRequest()
                    .index(marketCandleDataList.get(0).getMarket().toLowerCase())
                    .type(marketCandleDataList.get(0).getMarket().toLowerCase())
                    .source(getDataAsJSON(itr.next()), XContentType.JSON);

            bulkRequest.add(indexRequest);
        }
        try {
            BulkResponse br = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
            logger.info("Data saved to eS {}", br.iterator().next().getIndex());
        } catch (ElasticsearchException | IOException e) {
            e.printStackTrace();
            logger.info("exception at saving time " + e.getMessage());
        } catch (Exception e2) {
            e2.printStackTrace();
            logger.info("Common exception at saving time " + e2.getMessage());
        }

    }

    public String saveDataTest() {
        DataFile dataFile = new DataFile("3", "vipul");
        DataFile dataFile2 = new DataFile("4", "vipul2");
        dataFileRepo.save(dataFile);
        dataFileRepo.save(dataFile2);
        return dataFile.getId();
    }

    @Transactional
    public DataFile updateDataTest(long timeToPause) throws InterruptedException {
        logger.info("Request to update the entity");
        Optional<DataFile> second = dataFileRepo.findById("3");
        second.get().setName("vipulwwer" + LocalDate.now() + "--" + timeToPause);
        logger.debug(" current version {} {}", second.get().getVersion(), timeToPause);
        Thread.sleep(timeToPause);
        try {
            dataFileRepo.save(second.get());
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("error is {}", e);
        }
        logger.debug(" Data Saved for  version {} {}", second.get(), timeToPause);
        return second.get();
    }

    public Object sendMessage() {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(
                new com.twilio.type.PhoneNumber("whatsapp:+918368445172"),
                new com.twilio.type.PhoneNumber("whatsapp:+14155238886"),
                "Hi Joe! Thanks for placing an order with us. We’ll let you know once your order has been processed and delivered. Your order number is O12235234")
                .create();
        System.out.println(message.getStatus());
        return true;
    }
    @Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRED)
    public Object saveSales() throws Exception {
        Sales sales = new Sales();
        sales.setName("VVV");
        List<GraphDetails> graphDetailsList = new ArrayList<>();
        graphDetailsList.add(new GraphDetails("A"));
        graphDetailsList.add(new GraphDetails("B"));
        graphDetailsList.add(new GraphDetails("C"));
        sales.setGraphDetails(graphDetailsList);
        salesRepository.save(sales);
        logger.info("data {}", sales);
        if(true) throw new Exception("!!!", new Throwable("!!!"));
        Sales sales2 = new Sales();
        List<GraphDetails> graphDetailsList2 = new ArrayList<>();
        graphDetailsList2.add(new GraphDetails("A"));
        sales2.setGraphDetails(graphDetailsList2);
        salesRepository.save(sales2);
        return sales;
    }

    public Object updateSales(Sales sales) {
        List<GraphDetails> graphDetailsList = sales.getGraphDetails();
        graphDetailsList.add(new GraphDetails("D1"));
        graphDetailsList.add(new GraphDetails("E2"));
        sales = salesRepository.save(sales);
        logger.info("data {}", sales);
        return sales;
    }

    private List<Vehicle> getDefaultVehicle() {
        if (vehicles.isEmpty()) {
            vehicles.add(new Vehicle("1", "Hero Honda", "2 wheeler", 190, new Ticket("1", new Date(), 10)));
            vehicles.add(new Vehicle("2", "Bajaj", "2 wheeler", 120, new Ticket("11", new Date(), 10)));
            vehicles.add(new Vehicle("3", "TVS", "2 wheeler", 170, new Ticket("14", new Date(), 20)));
            vehicles.add(new Vehicle("4", "YAMAHA", "2 wheeler", 180, new Ticket("15", new Date(), 30)));
            vehicles.add(new Vehicle("5", "SUZUKI", "2 wheeler", 150, new Ticket("51", new Date(), 30)));

            vehicles.add(new Vehicle("6", "MARUTI", "4 wheeler", 350, new Ticket("12", new Date(), 110)));
            vehicles.add(new Vehicle("7", "BMW", "4 wheeler", 250, new Ticket("13", new Date(), 120)));
            vehicles.add(new Vehicle("8", "WolksWagen", "4 wheeler", 350, new Ticket("41", new Date(), 100)));
        }
        return vehicles;
    }

    private List<Vehicle> findVehicle(List<Vehicle> vehicleList, String type) {
        List<Vehicle> response = new ArrayList<>();
        vehicleList.forEach(current -> {
            if (current.getType().equalsIgnoreCase(type)) {
                response.add(current);
            }
        });
        System.out.format("%-15s");/// COMPLETE
        response.forEach(current -> {
            System.out.printf(current.toString());//
            System.out.printf("%-15s", current.getRegistrationNumber(), current.getName(), current.getType()); // Second
        });
        return response;
    }

    private List<Vehicle> findVehicleByParkedTime(List<Vehicle> vehicleList, Date parkedTime) {
        List<Vehicle> response = new ArrayList<>();
        vehicleList.forEach(current -> {
            if (current.getTicket().getParkedTime().equals(parkedTime)) {
                response.add(current);
            }
        });
        System.out.format("%-15s");/// COMPLETE
        response.forEach(current -> {
            System.out.printf(current.toString());//  this or below
            System.out.printf("%-15s", current.getRegistrationNumber(), current.getName(), current.getType()); // Second
        });
        return response;
    }

    private TreeMap<String, Integer> typeByCount(List<Vehicle> vehicleList) {
        TreeMap<String, Integer> response = new TreeMap<String, Integer>();
        vehicleList.forEach(current -> {
            if (response.containsKey(current.getType())) {
                Integer currentCount = response.get(current.getType());
                response.put(current.getType(), ++currentCount);
            } else {
                response.put(current.getType(), 1);
            }
        });
        return response;
    }

}
