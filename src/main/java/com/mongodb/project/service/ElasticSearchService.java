package com.mongodb.project.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.project.domain.MarketCandleData;
import com.mongodb.project.repo.DataFileRepo;
import com.mongodb.project.repo.MarketCandleDataRepository;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * CreatedBy vipulpandey on 6/5/19
 **/
@Service
public class ElasticSearchService {
    // "ETHUSD", "XRPUSD", "BCHUSD", "LTCUSD", "XLMUSD",
    //            "ZECUSD", "ETHBTC", "XRPBTC", "LTCBTC", "BCHBTC", "XLMBTC", "ZECBTC", "XLMETH"
    public static final String[] EXTERNAL_MARKET = {"BTCUSD"};
    public static final Integer ELASTIC_SEARCH_SIZE = 10_000;
    public static final String ONE_MONTH = "1M";
    public static final String FOURTEEN_DAYS = "14D";
    public static final String DAY_SUFFIX = "D";
    public static final String HOUR_SUFFIX = "h";
    public static final String MINUTE_SUFFIX = "m";

    public static final String[] ACTIVE_TIME_FRAMES = { 12 + HOUR_SUFFIX, 1 + DAY_SUFFIX,
            7 + DAY_SUFFIX, FOURTEEN_DAYS, ONE_MONTH};
    private static final Logger logger = LoggerFactory.getLogger(DataFileService.class);
    int j = 0;
    @Autowired
    DataFileRepo dataFileRepo;
    @Autowired
    RestHighLevelClient restHighLevelClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MarketCandleDataRepository marketCandleDataRepository;

    private List<MarketCandleData> getDataFromMysql(String market, String timeFrame, int skip, int limit) {
        return marketCandleDataRepository.findByMarketAndTimeInterval(market, timeFrame, skip, limit);
    }

    public Long syncDataInElasticSearch() {
        AtomicLong total = new AtomicLong();

        try {

            int pageSize = 10000;
            for (String market : EXTERNAL_MARKET) {
                Long currentMarketCount = new Long(0);
                for (String timeFrame : ACTIVE_TIME_FRAMES) {
                    if (timeFrame.equals("1m")) {
                        //timeFrame="1min";
                        continue;
                    }

                    int i = 0;
                    List<MarketCandleData> marketCandleData = getDataFromMysql(market, timeFrame, i, pageSize);
                    // added in main counter
                    total.getAndAdd(marketCandleData.size());

                    logger.info("Data found {} ", marketCandleData.size());

                    if (marketCandleData.size() > 0) {
                        //save to file
                        saveData(marketCandleData, market, timeFrame, i);
                        // increment i to size
                        i = i + marketCandleData.size();

                        currentMarketCount += marketCandleData.size();
                        total.getAndAdd(marketCandleData.size());
                        logger.info("counter inc to {} ", i);


                        while (marketCandleData.size() > 0) {
                            // again fetch from data base new records
                            marketCandleData.clear();  // cleared the data
                            marketCandleData = getDataFromMysql(market, timeFrame, i, pageSize);
                            if (marketCandleData.size() > 0) {
                                logger.info("More Data found  {} for {}", market, marketCandleData.size());
                                saveData(marketCandleData, market, timeFrame, i);
                                i = i + marketCandleData.size();
                                currentMarketCount += marketCandleData.size();
                            }
                            total.getAndAdd(marketCandleData.size());

                        }
                    }
                    logger.info("Data saved for {} {} {} ", currentMarketCount, timeFrame, market);
                }
                logger.info("Successfully written data for  {} {}", market, total.get());
            }
        } catch (Throwable e) {
            logger.info(e + "");
        }
        return total.get();
    }


    public void saveData(List<MarketCandleData> marketCandleDataList, String market, String timeFrame, int i) {
        saveDataInElasticSearch(marketCandleDataList,timeFrame);
//        String threadName = Thread.currentThread().getName();
//        System.out.println(threadName);
//        Iterator<MarketCandleData> itr = marketCandleDataList.iterator();
//        if (i % 80000 == 0) {
//            j++;
//        }
//        FileWriter file = null;
//        try {
//            File directory = new File("/opt/json/" + market);
//            if (!directory.exists()) {
//                directory.mkdir();
//            }
//            file = new FileWriter("/opt/json/" + market + "/" + market + "-" + timeFrame + "-" + j + ".json", true);
//
//            while (itr.hasNext()) {
//                org.json.simple.JSONObject jsonObject = getDataAsJSON(itr.next());
//                String str = "{\"index\": {\"_type\":\"" + market.toLowerCase() + "\"}}" + "\n" + jsonObject.toJSONString() + "\n";
//                file.write(str);
//            }
//        } catch (Exception e) {
//            logger.info("FOF");
//        } finally {
//            if (file != null) {
//                try {
//                    file.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
    }


    private org.json.simple.JSONObject getDataAsJSON(MarketCandleData marketCandleData) {
        org.json.simple.JSONObject response = new org.json.simple.JSONObject();
        try {
            response.put("market", marketCandleData.getMarket().toLowerCase());
            response.put("timeInterval", marketCandleData.getTimeInterval());
            response.put("timestamp", marketCandleData.getTimestamp().doubleValue());
            response.put("created", new Date(marketCandleData.getTimestamp()));
            response.put("open", marketCandleData.getOpen().doubleValue());
            response.put("high", marketCandleData.getHigh().doubleValue());
            response.put("close", marketCandleData.getClose().doubleValue());
            response.put("low", marketCandleData.getLow().doubleValue());
            response.put("volume", marketCandleData.getVolume().doubleValue());

        } catch (Exception e) {

        }
        return response;
    }
    public void saveDataInElasticSearch(List<MarketCandleData> marketCandleDataList,String timeFrame) {
        Iterator<MarketCandleData> itr = marketCandleDataList.iterator();
        BulkRequest bulkRequest = new BulkRequest();
        while (itr.hasNext()) {

            IndexRequest indexRequest = new IndexRequest()
                    .index(marketCandleDataList.get(0).getMarket().toLowerCase())
                    .type(marketCandleDataList.get(0).getMarket().toLowerCase())
                    .source(getDataAsJSON(itr.next()), XContentType.JSON);

            bulkRequest.add(indexRequest);
        }
        try {
            BulkResponse br = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
            logger.info("Data saved to eS {}", br.iterator().next().getIndex());
        } catch (ElasticsearchException | IOException e) {
            e.printStackTrace();
            logger.info("exception at saving time " + e.getMessage());
        } catch (Exception e2) {
            e2.printStackTrace();
            logger.info("Common exception at saving time " + e2.getMessage());
        }

    }
}
